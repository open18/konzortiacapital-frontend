const env = {
	development: {
		URL_API: 'http://localhost:4000/api/'
	},
	production: {
		URL_API: 'https://lit-plateau-08585.herokuapp.com/api/'
	}
};

export const URL_API = env[process.env.NODE_ENV].URL_API;
