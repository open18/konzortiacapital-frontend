import MaterialTable from 'material-table';
import React from 'react';
import axios from 'axios';
import { URL_API } from './../service/config';

const Clasification = () => {

    const [data, setData] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        getData();
    }, []);

    const onRowAddHandler = newData => {
        var config = {
            method: 'post',
            url: URL_API + 'clasification',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(newData)
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.log(error);
        });
    }

    const getData = () => {
        setLoading(true);
        var config = {
            method: 'get',
            url: URL_API + 'clasification'
        };

        axios(config).then(function (response) {
            setLoading(false);
            if (response.data.status)
                setData(response.data.data)
        }).catch(function (error) {
            setLoading(false);
            console.error(error);
        });

    }


    const onRowUpdateHandler = (newData, oldData) => {
        var data = JSON.stringify({
            "name": newData.name
        });

        var config = {
            method: 'put',
            url: URL_API + 'clasification/' + oldData._id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.error(error);
        });
    }

    const onRowDeleteHanlder = oldData => {
        var config = {
            method: 'delete',
            url: URL_API + 'clasification/' + oldData._id,
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.log(error);
        });
    }

    return (
        <MaterialTable
            isLoading={loading}
            title="Clasificaciones"
            columns={[
                { title: 'Nombre', field: 'name' }
            ]}
            data={data}
            actions={[
                {
                    icon: 'refresh',
                    tooltip: 'Recargar',
                    isFreeAction: true,
                    onClick: () => getData()
                }
            ]}
            editable={{
                onRowAdd: onRowAddHandler,
                onRowUpdate: onRowUpdateHandler,
                onRowDelete: onRowDeleteHanlder
            }}
            options={{
                pageSize: 10
            }}
        />
    );
};

export default Clasification;