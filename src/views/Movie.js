import MaterialTable from 'material-table';
import React from 'react';
import axios from 'axios';
import { URL_API } from './../service/config';

const Movie = () => {

    const [data, setData] = React.useState([]);
    const [dataClasification, setDataClasification] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        getData();
        getDataClasification();
    }, []);


    const getDataClasification = () => {
        setLoading(true);
        var config = {
            method: 'get',
            url: URL_API + 'clasification'
        };

        axios(config).then(function (response) {
            setLoading(false);
            if (response.data.status)
                setDataClasification(response.data.data)
        }).catch(function (error) {
            setLoading(false);
            console.error(error);
        });

    }

    const getData = () => {
        setLoading(true);
        var config = {
            method: 'get',
            url: URL_API + 'movie'
        };

        axios(config).then(function (response) {
            setLoading(false);
            if (response.data.status)
                setData(response.data.data)
        }).catch(function (error) {
            setLoading(false);
            console.error(error);
        });

    }

    const onRowAddHandler = newData => {
        var config = {
            method: 'post',
            url: URL_API + 'movie',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(newData)
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.log(error);
        });
    }




    const onRowUpdateHandler = (newData, oldData) => {
        var data = JSON.stringify({
            name: newData.name,
            director: newData.director,
            clasification: newData.clasification,
        });

        var config = {
            method: 'put',
            url: URL_API + 'movie/' + oldData._id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.error(error);
        });
    }

    const onRowDeleteHanlder = oldData => {
        var config = {
            method: 'delete',
            url: URL_API + 'movie/' + oldData._id,
        };

        return axios(config).then(function (response) {
            getData();
        }).catch(function (error) {
            console.log(error);
        });
    }

    var clasificationObj = {};

    for (const item of dataClasification) {
        clasificationObj[item._id] = item.name;
    }


    return (
        <MaterialTable
            isLoading={loading}
            title="Peliculas"
            columns={[
                { title: 'Nombre', field: 'name' },
                { title: 'Director', field: 'director' },
                { title: 'Clasificación', field: 'clasification', lookup: clasificationObj },
            ]}
            data={data}
            actions={[
                {
                    icon: 'refresh',
                    tooltip: 'Recargar',
                    isFreeAction: true,
                    onClick: () => getData()
                }
            ]}
            editable={{
                onRowAdd: onRowAddHandler,
                onRowUpdate: onRowUpdateHandler,
                onRowDelete: onRowDeleteHanlder
            }}
            options={{
                pageSize: 10
            }}
        />
    );
};

export default Movie;